import React, { Component } from 'react';
import './css/utility.css'; //import my css

import WeatherCard from './WeatherCard'; //Imported my custom weather component to be loaded into the main component

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="app-header">
          <h1 className="app-title">Openweather App</h1>
        </header>

        <div className="container">
          <WeatherCard /> 
        </div>
      </div>
    );
  }
}

export default App;

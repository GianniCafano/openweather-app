import React, { Component } from 'react';

import './css/main.css';

const apiKey = '61019eda5b4f780cb7f9e98a4f16be3e'; //made a variable which holds my api key for openweather
const url = `http://api.openweathermap.org/data/2.5/group?id=2655603,524901,2968815,2643743,3117735&units=metric&appid=${apiKey}`; //the url with the api number included

console.log(url);

class WeatherCard extends Component{ //Here I create a component called weather component


    constructor(){ //
        super();
        this.state = {
            weatherData: [], //array that holds city generic weather data
        };
    }

    //method which makes API call using fetch to get cities weather data
    getWeather(){
    fetch(url) //fetch method will not work on older browser versions
      .then((response) => response.json()) //get the response in JSON format
      .then((data) => { //recieve the data
          //console.log(url);    
        this.setState({
            weatherData: data.list, //add the data in the weatherData array
            //cityInfo: data.list[].weather
        })
      })
    }

    //function to convert UNIX timestamp to the current date and time
    //The function takes in a single argument which is the UNIX timestamp value
    convertUnix(timestamp){
        let timestampString = new Date(timestamp*1000);
        let year = timestampString.getFullYear();
        let month = timestampString.getMonth()+1;
        let date = timestampString.getDate()
        let hours = timestampString.getHours();
        let minutes = "0" + timestampString.getMinutes();
        let seconds = "0" + timestampString.getSeconds();
        let time = date+'/'+month+'/'+year+ '  -  ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
        return time;
    }

    //lifecycle method which only fires when component has fully loaded
    componentDidMount(){
      this.getWeather(); //Call the API when component has loaded
    }

    render(){ //render function renders JSX
        return(
            <div className="row marg-top-30">
            {  
                
                //I map through the array to get the values/objects in the weatherData array
                this.state.weatherData.map((city, index) => {
                    //sunriseTime & SunsetTime uses the convertUnix function to convert the unix timestamp
                    let sunriseTime = this.convertUnix(city.sys.sunrise); 
                    let sunsetTime = this.convertUnix(city.sys.sunset);
                    return (
                    <div key={index}>
                            <div className="col-md-4">
                                <div className="weather-card pad-10 marg-top-15">
                                    <h1 data-city-name={city.name} data-city-id={city.id} className="city-name"><a href="#" onClick={this.detectHeaderClick}>{city.name}</a></h1>
                                    <p>Sunrise: {sunriseTime}</p>
                                    <p>Sunset: {sunsetTime}</p>
                                    {
                                        city.weather.map((weatherInfo, i) => {
                                            return(
                                                <div key={i}>
                                                <img src={"http://openweathermap.org/img/w/"+weatherInfo.icon+".png"}></img>
                                                    <p>Weather Type: {weatherInfo.main}</p>
                                                    <p>Weather Description: {weatherInfo.description}</p>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                    </div>
                    )   
                })
                
            }


            {/* TODO: CREATE SEPARATE ARRAY TO LOOP WEATHER PROPERTY IN JSON ARRAY*/}
            </div>
        );
    }
}

//exports component to be seen by other components
export default WeatherCard;